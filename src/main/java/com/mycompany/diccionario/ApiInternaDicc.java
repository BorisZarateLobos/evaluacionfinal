/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.diccionario;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Admin
 */

@Path("diccionario")
public class ApiInternaDicc {
    
    @GET
    @Path("/(idbuscar)")
    @Produces(MediaType.APPLICATION_JSON)
    
    public Response significado(@PathParam("idbuscar") String idbuscar) {
        
        Client client = ClientBuilder.newClient();
        
        WebTarget myResource = client.target("https://cd-api.oxforddictionaries.com:443/api/v2/entries/es/" + idbuscar);
        return null;
        
    }
            
  
    
}
