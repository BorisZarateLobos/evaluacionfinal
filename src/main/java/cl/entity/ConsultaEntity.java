/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.entity;

/**
 *
 * @author Admin
 */
public class ConsultaEntity {

    /**
     * @return the palabra
     */
    public String getPalabra() {
        return palabra;
    }

    /**
     * @param palabra the palabra to set
     */
    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the significado
     */
    public String getSignificado() {
        return significado;
    }

    /**
     * @param significado the significado to set
     */
    public void setSignificado(String significado) {
        this.significado = significado;
    }
    
    private String palabra;
    private String fecha;
    private String significado;
    
}
